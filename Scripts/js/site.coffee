
(($, window, undefined_) ->
  "use strict"
  Modernizr = window.Modernizr
  $(document).ready ->
    
    
    $("#nav-toggle").on "touchstart click", (e) ->
      e.preventDefault()
      $body = $("body")
      $page = $(".page-container")
      $menu = $("#mobile-nav")
      
      transitionEnd = "transitionend webkitTransitionEnd otransitionend MSTransitionEnd"
      $body.addClass "animating"
      
      if $body.hasClass("nav-open")
        $body.addClass "right"
      else
        $body.addClass "left"
      
      $page.on transitionEnd, ->
        $body.removeClass("animating left right").toggleClass "nav-open"
        $page.off transitionEnd
        return

      return


    $("img.fade-image").lazyload
      effect: "fadeIn"
    

    pdfIcon = $(".icon-pdf")
    pdfIconTop = 0
    if pdfIcon.length
      pdfIconPos = pdfIcon.offset()
      pdfIconTop = pdfIconPos.top

    $(window).scroll ->
      curPos = $(this).scrollTop()
      if curPos > pdfIconTop
        pdfIcon.delay(1000).addClass("active")
        $('.btn-download').delay(950).addClass("active")

    $(".btn-top").click ->
      $("html, body").animate
        scrollTop: 0
      , 800
      false

    $(".lnk-contact a").on 'click', (e) ->
      e.preventDefault()
      $('body').removeClass('nav-open')
      $("html, body").animate
        scrollTop: $('#contact-us').offset().top
      , 500
      $('.btn-contact').trigger('click')
      false
    
    contactForm = $('#contact_form')
    $('.btn-contact').on 'click', (e) ->
      e.preventDefault()
      if $(this).hasClass('active')
        contactForm.slideUp()
        $(this).removeClass('active')
      else
        contactForm.slideDown();
        $(this).addClass('active')

    $(window).resize ->
      windowWidth = $(window).width()
      if windowWidth > 769
        $('body').removeClass('nav-open')





  # End of $(document).ready
  
  # UNCOMMENT THE LINE YOU WANT BELOW IF YOU WANT IE8 SUPPORT AND ARE USING .block-grids.
  $(".block-grid.two-up>li:nth-child(2n+1)").css clear: "both"
  $(".block-grid.three-up>li:nth-child(3n+1)").css clear: "both"
  $(".block-grid.four-up>li:nth-child(4n+1)").css clear: "both"
  $(".block-grid.five-up>li:nth-child(5n+1)").css clear: "both"
  
  # Hide address bar on mobile devices (except if #hash present, so we don't mess up deep linking).
  if Modernizr.touch and not window.location.hash
    $(window).load ->
      setTimeout (->
        window.scrollTo 0, 1
        return
      ), 0
      return

  return
) jQuery, this

		