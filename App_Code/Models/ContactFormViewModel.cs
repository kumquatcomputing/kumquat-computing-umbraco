﻿using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class ContactFormViewModel
    {
        [Required(ErrorMessage = " is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = " is required")]
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string CompanyName { get; set; }
        public string Message { get; set; }        
    }    
}




//namespace Models
//{

//    public class ContactFormViewModel : RenderModel
//    {
//        public ContactFormViewModel()
//            : this(new UmbracoHelper(UmbracoContext.Current).TypedContent(UmbracoContext.Current.PageId))
//        {
//        }

//        public ContactFormViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
//        {
//        }

//        public ContactFormViewModel(IPublishedContent content) : base(content)
//        {
//        }


//        [Required(ErrorMessage = " is required")]
//        public string Name { get; set; }

//        [Required(ErrorMessage = " is required")]
//        public string Email { get; set; }

//        public string Phone { get; set; }
//        public string Skype { get; set; }
//        public string CompanyName { get; set; }
//        public string Message { get; set; }

//    }
//}