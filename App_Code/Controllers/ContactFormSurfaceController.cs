﻿using System.Configuration;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using Models;
using Umbraco.Web.Mvc;

public class ContactFormSurfaceController : SurfaceController
{

    [HttpPost]
    public ActionResult ContactForm(ContactFormViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return RedirectToCurrentUmbracoPage();
        }

        var sb = new StringBuilder();
        //sb.AppendFormat("Message: {0}", model.Subject);
        sb.AppendFormat("Name: {0}", model.Name + "<br/>");
        sb.AppendFormat("Email: {0}", model.Email + "<br/>");
        sb.AppendFormat("Phone: {0}", model.Phone + "<br/>");
        sb.AppendFormat("Skype: {0}", model.Skype + "<br/>");
        sb.AppendFormat("Company Name: {0}", model.CompanyName + "<br/>" + "<br/>");
        sb.AppendFormat("Message: {0}", model.Message + "<br/>");

        MailMessage msg = new MailMessage();
        SmtpClient smtp = new SmtpClient();
        MailAddress from = new MailAddress("info@kumquatcomputing.co.uk");
        //MailAddress from = new MailAddress("gareth.edmunds@kumquatcomputing.co.uk");

        // msg.To.Add("info@kumquatcomputing.co.uk");
        msg.To.Add("gareth.edmunds@kumquatcomputing.co.uk");        

        msg.From = from;
        msg.Subject = "Contact Us";
        msg.IsBodyHtml = false;
        smtp.Host = "smtp.gmail.com";
        //smtp.Port = 465;
        smtp.Port = 587;

        smtp.Credentials = new System.Net.NetworkCredential("info@kumquatcomputing.co.uk", "kumquat.12_87!2");
        //smtp.Credentials = new System.Net.NetworkCredential("gareth.edmunds@kumquatcomputing.co.uk", "bacon1981");
        smtp.EnableSsl = true;
        msg.Subject = "Form Submission Received";
        msg.Body = sb.ToString();
        msg.IsBodyHtml = true;

        try
        {
            smtp.Send(msg);
            //return RedirectToCurrentUmbracoPage(/home/contact-form-pages/success/); //TODO: Change this to thank you page            
            return RedirectToUmbracoPage(1132);
        }
        catch (SmtpException smtpEx)
        {
            // Log or manage your error here, then...
            //return RedirectToUmbracoPage(1173);
            return RedirectToUmbracoPage(1238);// <- currentumbracopage="" error="" model.redirectpage="" my="" page.="" pre="" published="" redirecttoumbracopage="" return="">
        }
    }
}
